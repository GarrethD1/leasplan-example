Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios
  @GetProductFeature
  Scenario Outline:Product search
    Given he has the endpoint "BASE_URL"
    When he calls endpoint with product "<ProductName>"
    Then he sees the results displayed for "<ProductName>"
    Examples:
      |ProductName|
      | apple     |
      | mango     |
      | tofu      |
      | water     |

  Scenario Outline:Product search negative
    Given he has the endpoint "BASE_URL"
    When he calls endpoint with product "<ProductName>"
    Then he does not see the results
    Examples:
      |      ProductName      |
      | car                   |
      | bike                  |
      | Yu-gi-oh Booster pack |