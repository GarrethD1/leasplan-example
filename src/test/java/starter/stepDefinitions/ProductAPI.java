package starter.stepDefinitions;


import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ProductAPI {

@Step
    public void CallEndpoint(String url)
    {
        Response response = SerenityRest.given().get(url);
    }

}
