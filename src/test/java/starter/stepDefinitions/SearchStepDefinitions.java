package starter.stepDefinitions;


import Reusables.URL;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;


import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;


public class SearchStepDefinitions {
    private String BaseUrl;

    @Steps
    public ProductAPI productAPI;

    @Given("he has the endpoint {string}")
    public void heHasTheEndPoint(String url) {
        BaseUrl = URL.getTranslatedEnum(url);

    }
    @When("he calls endpoint with product {string}")
    public void heCallsProductEndpoint(String productName) {
        productAPI.CallEndpoint(BaseUrl + productName);

    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForApple(String productName) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(lastResponse->lastResponse().jsonPath().getString("title").contains(productName));

    }
    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }


//    @Then("he sees the results displayed for mango")
//    public void heSeesTheResultsDisplayedForMango() {
//        restAssuredThat(lastResponse->lastResponse().jsonPath().getString("title").contains("mango"));
//    }


}
