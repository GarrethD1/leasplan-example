package Reusables;

public enum URL {
    BASE_URL("https://waarkoop-server.herokuapp.com/api/v1/search/test/");

    private String url;

    URL(String url)
    {
        this.url = url;
    }
    public static String getTranslatedEnum(String url) {
        URL nameToTranslate = URL.valueOf(url.toUpperCase());
        return nameToTranslate.toString();
    }
    @Override
    public String toString() {
        return url;
    }
}
