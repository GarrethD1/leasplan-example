# Project Description:

Project was set up using Serenity and RestAssured
Written in Java with Junit, Cucumber & maven
Project was developed using IntelliJ IDE with the Cucumber plugin

# Setup:

1. Install [Java 11.0.5+](https://www.oracle.com/za/java/technologies/javase/javase-jdk8-downloads.html)
2. Install Maven [Maven](https://maven.apache.org/download.cgi)
3. Install IntelliJ if Desired for minimal issues.
4. Set up environment variables. [Setup instructions](https://docs.oracle.com/en/database/oracle/machine-learning/oml4r/1.5.1/oread/creating-and-modifying-environment-variables-on-windows.html) It's important to note that the most important environment variables right now is JAVA_HOME and MAVEN_HOME.


# Run tests:

**There are 2 ways of running tests**


1.  Navigate to the root of the project file and in command line and type in `mvn clean verify`
- This will run ALL the feature files within the resource/features package.
- This method does not allow flexible debugging.
- It allows for running tests through maven

2. Open the project in java IDE of choice (IntelliJ is best ;-D) and Right click > Run/debug > On the Feature file of choice.
- This will allow one to either debug or run the test from end to end with a report generated
- This gives more control as to what should run and what should not.
- This allows a user to run a test via maven.


# Reporting

- HTML report will be generated once the tests are finished executing.
- These reports can be found at: `Root\target\site\serenity`
- Open Index.html in browser to see the reports

# How to Write Test case Scenarios

**TestRunners**
1. The TestRunners Package is where we keep the runners of the Serenity Cucumber framework. In the runner class we can define the Feature file to use.
```
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"src/test/resources/features/"},glue={"java","StepDefinitions"})
```

- If you add your own TestRunner, make sure to change feature path to point to the correct Feature file or folder
  `{"src/test/resources/features/"}`

**StepDefinitions**
- In the StepDefinitions package we add the annotations `@Given`, `@When`, `@Then`
- A Step Definition is a Java method with an expression that links it to one or more Gherkin steps. When Cucumber executes a Gherkin step in a scenario, it will look for a matching step definition to execute.
 This is where we add all the code for API calls using native java or SerenityRest code that the business usually don't care about. Here we can be as clean or as messy as we need to be but generaly speaking, clean readable code is good practice.

# GitLab CI pipeline
The Gitlab CI pipeline is fully functional using the gitLab Public runners. What this means is, you need to download this repo and upload it as your own onto gitlab.
Once this is done you need to go into Settings > CI/CD > Runners and specify the `.gitlab-ci.yml` file in the dropdown area of the runners settings.
In most cases the settings part is not necessary and GitLab will see the `.yml `file in the root of your Repo. Every time you commit changes to the repo the runner will run the job and run through the pipeline to ensure no broken code was submited.


#Updates and changes:

- Created an Enum class to store URLs that are used across the assessment to minimize duplicate code.
- Changed the Feature file wording a little to support the change of the endpoint URL.
- Completely reImported all maven dependencies as a lot of dependencies where outdated and where causing compatibility issues.
- Spelling error in the feature file was corrected .
- Removed duplicate maven dependencies in the pom.
- updates all the pom file dependencies to the latest stable versions.


###Class changes
- Changes the stepDefinitions *carsAPI* to *ProductAPI*
- The code for getting the endpoint data was moved from the *SearchStepDefinition* class to the *ProductAPI* class.
- Method name change from under-score to camel case to keep things as consistent as possible.
```java
   @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("error", contains("True")));
```
to
```java
    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
```
- Shifted the logic for calling the API out of the _StepDefinition_ class into the ProductAPI class to keep the _StepDefinition_ as clean as possible.

```java
    public void CallEndpoint(String url)
    {
        Response response = SerenityRest.given().get(url);
        System.out.println(response.getBody().toString()); //debug purpose
    }
```
- Added Given step for both positive and negative scenarios
- Added Test data tables to minimize the copying of variables
- Added incorrect search terms to validate the products don't exist
- Added an Enum string translator so that enums could be used within feature files.




